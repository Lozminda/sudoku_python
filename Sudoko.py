

class a_square:
    '''
    A single Sudoku square, answers will be a list containing a range
    1 to and including 9 as possible answers. When the list gets down
    to 1 element, then that'll be that square solved. And as possible
    answers are whittled away, thus answers elements will be deleted.
    '''
    def __init__(self,  answers, row, col):
        self.answers = answers
        self.row = row
        self.col = col

class Board:
    '''
    The main class containing all 81 squares and will contain other
    methods such as Solve() etc. Probably some more 'private' methods
    too
    '''
    SQUARES_PER_SIDE = 9
    all_squares = []

    def __init__(self):
        '''
        Constructor reads in 9 user strings, each character/number being
        the number in that column. Each string being nine characters and
        thus nine squares. Input a string nine times and there's the 81
        squares in a sudoku board. Any character can be used to signify an
        unknown or empty square. Eg:
               "12---4---"
               "--5--6-8-"
               "----6579-"
               "---------"
               "--1-----2"
               "---------"
               "6---8-23-"
               "---------"
               "---------"
        '''
# Can't load and save at the same time
        flag = None
        pre_saved = input("Load a pre-saved board (y/n): ")
        if pre_saved is "y":
            flag = "r"

        else:
            choice = input("Do you want to save this board for later (y/n): ")
            if choice is "y":
                flag = "w"

        self.__recieve_rows_of_squares(flag)

    def __recieve_rows_of_squares(self, flag):
        '''
        Each row of squares is represented by a string, each character being
        the value of the square. 'Row Strings' can be either loaded from pre-
        existing file or saved to a file or just inputed with no file handling:
        flag will be passed to open() and be "w"/"r"/None.
        Files are in directory 'boards/' and have file ext '.sod'
        '''
        if flag is not None:
            stateStr = "LOADED"
            if flag == "w":
                stateStr = "saved"
            boardname = input("Please input board name to be " + stateStr + ": ")
            boardname = "./boards/" + boardname + ".sod"
            board_file = open(boardname, flag)

# DON'T THINK I CAN USE THIS open('output.txt', flag) as board_file:

# Reading input strings and adding squares to the list of all_squares
        row_count = 0
        while(row_count < self.SQUARES_PER_SIDE):

            if flag is "r":
                digitsStr = board_file.readline().rstrip()

            else:
                digitsStr = input(("Digits for row {} ".format(row_count+1)))

            if len(digitsStr) != self.SQUARES_PER_SIDE:
                print("input for row should be 9 digits long")
            else:
                if flag is "w":
                    board_file.writelines((digitsStr + "\n"))
                self.__turn_string_into_squares(row_count, digitsStr)
                row_count += 1

        if flag is not None:
            board_file.close()

    def __turn_string_into_squares(self, a_row, a_string):

# You can have any char as a space eg 1..2--0%5 will => 100200005
            column = 0
            for a_numStr in a_string:
                temp = a_square([range(1, 10)], a_row, column)
                if a_numStr.isdigit():
                    if a_numStr is not "0": # Zeros can be blank squares too
                        temp = a_square([int(a_numStr)], a_row, column)
                self.all_squares.append(temp)
                column += 1

    def Solve(self):
        '''
        Solves the sudoku !
        '''
        while not self.__all_squares_have_a_solution():
            pass

        # for r in range (0, self.SQUARES_PER_SIDE):
        #     print(self.all_squares[r:self.SQUARES_PER_SIDE].self.answers, end = " ")
        #     print()
        # print [self.all_suqares.a_square.answers for self.a_square in x]
        row = 0
        for square in self.all_squares:
            print( *square.answers, end =' ' )
            row += 1
            if row % self.SQUARES_PER_SIDE == 0:
                print()
                
    def __all_squares_have_a_solution(self) -> bool:
        return True


a_board = Board()
a_board.Solve()
print("finished")